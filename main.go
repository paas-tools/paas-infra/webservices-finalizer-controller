/*
Copyright 2017 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"net/http"
	"os"
	"time"

	flag "github.com/spf13/pflag"

	klog "github.com/sirupsen/logrus"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/util/runtime"
	"k8s.io/apimachinery/pkg/util/wait"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/util/retry"
	"k8s.io/client-go/util/workqueue"
)

// Constants for this controller
const (
	webServicesFinalizer          = "cern.ch/webservices-finalizer"
	webServicesDeleteSiteEndpoint = "https://webadminservices-iis7.web.cern.ch/WebAdminServices-iis7/FIMWebMgmt.asmx/DeleteWebsite"
	webServicesGetSiteEndpoint    = "https://webadminservices-iis7.web.cern.ch/WebAdminServices-iis7/FIMWebMgmt.asmx/GetDetailsFromFIM"
	webServicesQueryParam         = "SiteName"
)

var webServicesAdminUsername, webServicesAdminPassword string

var Config *tls.Config

// Dry run mode
var dryRun bool

type Controller struct {
	indexer  cache.Indexer
	queue    workqueue.RateLimitingInterface
	informer cache.Controller
	client   kubernetes.Interface
}

func NewController(queue workqueue.RateLimitingInterface, indexer cache.Indexer, informer cache.Controller, client kubernetes.Interface) *Controller {
	return &Controller{
		informer: informer,
		indexer:  indexer,
		queue:    queue,
		client:   client,
	}
}

func (c *Controller) processNextItem() bool {
	// Wait until there is a new item in the working queue
	key, quit := c.queue.Get()
	if quit {
		return false
	}
	// Tell the queue that we are done with processing this key. This unblocks the key for other workers
	// This allows safe parallel processing because two objects with the same key are never processed in
	// parallel.
	defer c.queue.Done(key)

	// Invoke the method containing the business logic
	err := c.doWork(key.(string))
	// Handle the error if something went wrong during the execution of the business logic
	c.handleErr(err, key)
	return true
}

// doWork is the business logic of the controller. In case an error happened, it has to simply return the error.
// The retry logic should not be part of the business logic.
func (c *Controller) doWork(key string) error {
	obj, exists, err := c.indexer.GetByKey(key)
	if err != nil {
		klog.Errorf("Fetching object with key %s from store failed with %v", key, err)
		return err
	}

	if !exists {
		klog.Infof("Namespace %s does not exist anymore\n", key)
		return nil
	}

	namespace := obj.(*v1.Namespace)
	// Use a bool flag to decide if the object needs to be changed or not
	needToUpdate := false

	klog.Infof("Processing namespace '%s'", namespace.GetName())
	// Check if the namespace is owned by webservices
	if _, ok := namespace.Labels["cern.ch/webservices-owner"]; ok {
		// Check if the namespace is set to be deleted
		if namespace.DeletionTimestamp != nil {
			// Generate a slice of finalizers excluding the one to delete
			klog.Infof("Namespace '%s' is managed by CERN web services and has been deleted", namespace.GetName())
			newFinalizers := []string{}
			for _, finalizer := range namespace.GetFinalizers() {
				// If the finalizer for unregistering from web services is present, proceed
				if finalizer == webServicesFinalizer {
					if err := unregisterFromWebservices(namespace.GetName(), dryRun); err != nil {
						return err
					}
					klog.Infof("Site '%s' does not exists anymore", namespace.GetName())
					needToUpdate = true
				} else {
					// If the finalizer is not the one we are looking for, add it to the
					// new list of finalizers
					newFinalizers = append(newFinalizers, finalizer)
				}
			}
			// Overwrite the finalizers with the changes
			namespace.SetFinalizers(newFinalizers)
		}
	} else {
		klog.Infof("Namespace '%s'  NOT present in webservices, ignoring...\n", namespace.GetName())
	}
	// If the namespace was changed, save the object
	if needToUpdate && !dryRun {
		if err := c.updateNamespace(namespace); err != nil {
			return err
		}
	}
	return nil
}

func (c *Controller) updateNamespace(namespace *v1.Namespace) error {
	finalizers := namespace.GetFinalizers()
	fmt.Printf("Namespace '%s' was changed, updating...\n", namespace.GetName())
	retryErr := retry.RetryOnConflict(retry.DefaultRetry, func() error {
		// Retrieve the latest version of Namespace before attempting update
		// RetryOnConflict uses exponential backoff to avoid exhausting the apiserver
		namespaceClient := c.client.CoreV1().Namespaces()

		result, getErr := namespaceClient.Get(namespace.GetName(), metav1.GetOptions{})
		if getErr != nil {
			panic(fmt.Errorf("Failed to get latest version of Namespace: %v", getErr))
		}
		result.SetFinalizers(finalizers)
		_, updateErr := namespaceClient.Update(result)
		return updateErr
	})
	if retryErr != nil {
		return fmt.Errorf("error updating namespace '%s', aborting", namespace.GetName())
	}
	fmt.Printf("Namespace '%s' successfully updated!\n", namespace.GetName())
	return nil
}

// Delete the site from web services by using the rest API provided
func unregisterFromWebservices(namespace string, dryRun bool) error {
	// Use an HTTP client instead of a single request as we need to add authentication
	// and params

	// First check the site actually exists in FIM
	statusCodeGet, err := prepareQueryToWebServices(webServicesGetSiteEndpoint, namespace, dryRun)
	if err != nil {
		return err
	}

	if statusCodeGet != 200 {
		// The site does not exist anymore, continue as expected
		klog.Infof("The Site %s does not exist in web services DB. Status code: %d", namespace, statusCodeGet)
		return nil
	}
	// Once we know the site exists, carry on deleting it
	statusCodeDelete, err := prepareQueryToWebServices(webServicesDeleteSiteEndpoint, namespace, dryRun)
	if err != nil {
		return err
	}
	if statusCodeDelete != 200 {
		return fmt.Errorf("ERROR: The API call to %s failed. Status code %d", webServicesDeleteSiteEndpoint, statusCodeDelete)
	}
	klog.Infof("Site '%s' successfully removed from web services!", namespace)
	return nil
}

func prepareQueryToWebServices(url string, namespace string, dryRun bool) (int, error) {
	tr := &http.Transport{TLSClientConfig: Config}

	client := &http.Client{
		Transport: tr,
		Timeout: time.Second * 10,
	}
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return 0, err
	}
	// Set the site to be deleted as a parameter of the request
	q := req.URL.Query()
	q.Add(webServicesQueryParam, namespace)
	req.URL.RawQuery = q.Encode()
	// Obtain credentials to delete the site from environment variables
	req.SetBasicAuth(os.Getenv("WEBSERVICES_ADMIN_USERNAME"), os.Getenv("WEBSERVICES_ADMIN_PASSWORD"))
	if dryRun {
		klog.Infof("Dry-Run: Request to '%s' skipped...", req.URL.String())
		return 200, nil
	}
	// Execute the request
	klog.Infof("Running query: %s", req.URL.String())
	resp, err := client.Do(req)
	if err != nil {
		return 0, err
	}
	defer resp.Body.Close()
	return resp.StatusCode, nil
}

// handleErr checks if an error happened and makes sure we will retry later.
func (c *Controller) handleErr(err error, key interface{}) {
	if err == nil {
		// Forget about the #AddRateLimited history of the key on every successful synchronization.
		// This ensures that future processing of updates for this key is not delayed because of
		// an outdated error history.
		c.queue.Forget(key)
		return
	}

	// This controller retries 5 times if something goes wrong. After that, it stops trying.
	if c.queue.NumRequeues(key) < 5 {
		klog.Infof("Error syncing object %v: %v", key, err)

		// Re-enqueue the key rate limited. Based on the rate limiter on the
		// queue and the re-enqueue history, the key will be processed later again.
		c.queue.AddRateLimited(key)
		return
	}

	c.queue.Forget(key)
	// Report to an external entity that, even after several retries, we could not successfully process this key
	runtime.HandleError(err)
	klog.Infof("Dropping Object %q out of the queue: %v", key, err)
}

func (c *Controller) Run(threadiness int, stopCh chan struct{}) {
	defer runtime.HandleCrash()

	// Let the workers stop when we are done
	defer c.queue.ShutDown()
	klog.Info("Starting Kubernetes controller")

	go c.informer.Run(stopCh)

	// Wait for all involved caches to be synced, before processing items from the queue is started
	if !cache.WaitForCacheSync(stopCh, c.informer.HasSynced) {
		runtime.HandleError(fmt.Errorf("Timed out waiting for caches to sync"))
		return
	}

	for i := 0; i < threadiness; i++ {
		go wait.Until(c.runWorker, time.Second, stopCh)
	}

	<-stopCh
	klog.Info("Stopping Kubernetes controller")
}

func (c *Controller) runWorker() {
	for c.processNextItem() {
	}
}

func main() {
	flag.BoolVar(&dryRun, "dry-run", false, "Set flag to only print the operations instead of actually doing them")
	flag.Parse()

	// Get the SystemCertPool, continue with an empty pool on error
	rootCAs, _ := x509.SystemCertPool()
	if rootCAs == nil {
		rootCAs = x509.NewCertPool()
	}

	// Trust the augmented cert pool in our client
	Config = &tls.Config{
		RootCAs: rootCAs,
	}

	// creates the connection
	config, err := rest.InClusterConfig()
	if err != nil {
		klog.Fatal(err)
	}

	// creates the clientset
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		klog.Fatal(err)
	}

	// create the Namespace watcher
	namespaceListWatcher := cache.NewListWatchFromClient(clientset.CoreV1().RESTClient(), "namespaces", "", fields.Everything())
	fmt.Println("Watching Namespaces")

	if dryRun {
		fmt.Println("Running in dry-run mode, operations will not change data.")
	}

	// create the workqueue
	queue := workqueue.NewRateLimitingQueue(workqueue.DefaultControllerRateLimiter())

	// Bind the workqueue to a cache with the help of an informer. This way we make sure that
	// whenever the cache is updated, the Object key is added to the workqueue.
	// Note that when we finally process the item from the workqueue, we might see a newer version
	// of the Object than the version which was responsible for triggering the update.
	indexer, informer := cache.NewIndexerInformer(namespaceListWatcher, &v1.Namespace{}, 0, cache.ResourceEventHandlerFuncs{
		AddFunc: func(obj interface{}) {
			key, err := cache.MetaNamespaceKeyFunc(obj)
			if err == nil {
				queue.Add(key)
			}
		},
		UpdateFunc: func(old interface{}, new interface{}) {
			key, err := cache.MetaNamespaceKeyFunc(new)
			if err == nil {
				queue.Add(key)
			}
		},
		DeleteFunc: func(obj interface{}) {
			// IndexerInformer uses a delta queue, therefore for deletes we have to use this
			// key function.
			key, err := cache.DeletionHandlingMetaNamespaceKeyFunc(obj)
			if err == nil {
				queue.Add(key)
			}
		},
	}, cache.Indexers{})

	controller := NewController(queue, indexer, informer, clientset)

	// Now let's start the controller
	stop := make(chan struct{})
	defer close(stop)
	go controller.Run(1, stop)

	// Wait forever
	select {}
}
