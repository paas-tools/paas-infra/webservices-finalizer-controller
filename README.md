# Web Services finalizer controller

This go controller takes care of unregistering from [CERN Web Services](cern.ch/web) sites
that were manually removed either through the OpenShift API (with the `oc` CLI)
or with the Web Console.

### Deployment
This controller is deployed in both `openshift.cern.ch` and `openshift-dev.cern.ch` in the `paas-infra` namespace.
For instructions on how to deploy the namespace, check the [OneNote docs](https://espace.cern.ch/openshift-internal/_layouts/15/WopiFrame.aspx?sourcedoc=%2Fopenshift%2Dinternal%2FShared%20Documents%2FOpenshift&action=edit&wd=target%28%2F%2FDeployment.one%7Cdacb26c0-65a0-4eee-8b63-99cfeaef4a66%2F%27paas-infra%27%20namespace%7C9a78943f-4902-48c6-a972-2a5b18e8e901%2F%29)

The account running the webservices-finalizer-controller needs permissions to edit all namespaces,
so we are going to assign it the `cluster-admin` role.
```
oc create serviceaccount webservices-finalizer-controller -n paas-infra
oc adm policy add-cluster-role-to-user cluster-admin -z webservices-finalizer-controller -n paas-infra
```

This controller directly interacts with the CERN Web Services API to delete websites. To do this,
we use a service account provided by the CERN web services team. After retriving the username
and password from one of the Web Services admins, create the following configmap:
```
oc create secret generic webservices-serviceaccount --type="cern.ch/webservices-serviceaccount" --from-literal=username=${WEBSERVICES_ADMIN_USERNAME} --from-literal=password=${WEBSERVICES_ADMIN_PASSWORD}
```

The values for `WEBSERVICES_ADMIN_USERNAME` and `WEBSERVICES_ADMIN_PASSWORD` are stored
as Secure variables in this repository.

Also, as the CERN web services API uses a CERN certificate, we need to include the
CERN CA certificate to verify the connection. From a machine with the CERN CA installed, run:
```
oc create configmap cern-ca-certificates --from-file=/etc/pki/tls/certs/CERN-bundle.pem
```

Once this is done, just create the application by running:
```
oc create -f deploy/ -n paas-infra
```

### Continuous Integration

The deployment of the application is managed by GitLab-CI. Any changes to the definition and config
files will be automatically redeployed into dev and production when merged to `master`.

For the development purposes, two manual triggers are included in the CI definition to allow easy deployment of
custom branches into the application running in the dev [environment](https://openshift-dev.cern.ch).

Check the [OneNote docs](https://espace.cern.ch/openshift-internal/_layouts/15/WopiFrame.aspx?sourcedoc=%2Fopenshift%2Dinternal%2FShared%20Documents%2FOpenshift&action=edit&wd=target%28%2F%2FDeployment.one%7Cdacb26c0-65a0-4eee-8b63-99cfeaef4a66%2F%27paas-infra%27%20namespace%7C9a78943f-4902-48c6-a972-2a5b18e8e901%2F%29) for instructions on how to set up the Continuos Integration and Continuous Deployment for this project.
