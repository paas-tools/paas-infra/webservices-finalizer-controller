FROM golang:1.13-alpine AS builder

# Install git + SSL ca certificates.
# Git is required for fetching the dependencies.
# Ca-certificates is required to call HTTPS endpoints.
RUN apk update && apk add --no-cache git ca-certificates && update-ca-certificates

# Copy the code from the host and compile it
WORKDIR $GOPATH/src/webservices-finalizer-controller
COPY . ./
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -mod=vendor -o /main .


FROM scratch
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /main ./

ENTRYPOINT ["./main"]



